from django.contrib import admin
from  accounts.models import  User
from  django.contrib.auth.admin import UserAdmin
from  accounts.forms import  UserCreationForm,UserChangeForm


class UserAdmin(UserAdmin):
    add_form =  UserCreationForm

    list_display = ('name','username', 'email', 'is_active')
    list_filter = ('is_active','name','email','username')

    fieldsets = (
        (None, {'fields': ('username', 'email','password')}),

        ('Permissions', {'fields': ('is_staff','is_active',)}),
    )

    search_fields =  ('username', )
    ordering = ('username',)

    filter_horizontal = ()

admin.site.register(User,UserAdmin)

