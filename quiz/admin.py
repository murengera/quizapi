from django.contrib import admin
from .models import *
from  django.db.models.signals import pre_save
from  django.dispatch import  receiver
from nested_admin.nested import NestedTabularInline,NestedModelAdmin,NestedInlineModelAdmin,ModelAdmin
from  django.template.defaultfilters import slugify



class AnswerInline(NestedTabularInline):
    model = Answer
    extra = 4
    max_num = 4

class QuestionInLine(NestedTabularInline):
    model = Question
    inlines = [AnswerInline,]
    extra = 5

class QuizAdmin(NestedModelAdmin):

    inlines = [QuestionInLine,]


class UserSAnswerInline(NestedTabularInline):
    model = UserAnswer

class QuizTakerAdmin(ModelAdmin):
    inlines = [UserSAnswerInline,]



admin.site.register(Quiz,QuizAdmin)

admin.site.register(Question)

admin.site.register(Answer)

admin.site.register(QuizTaker,QuizTakerAdmin)


admin.site.register(UserAnswer)


